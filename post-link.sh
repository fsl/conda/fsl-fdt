if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper bedpost bedpostx bedpostx_datacheck bedpostx_gpu bedpostx_postproc.sh bedpostx_postproc_gpu.sh bedpostx_preproc.sh bedpostx_single_slice.sh ccops dtifit dtigen eddy_combine eddy_correct fdt_rotate_bvecs make_dyadic_vectors maskdyads medianfilter merge_parts_gpu probtrack probtrackx pvmfit select_dwi_vols split_parts_gpu vecreg xfibres xfibres_gpu zeropad Fdt_gui
fi
